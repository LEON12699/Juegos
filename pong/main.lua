-- @Author: Your name
-- @Date:   2020-04-12 23:03:10
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-19 00:11:37
push = require 'push'
Class= require 'class'

require 'Paddle'

require 'Ball'

love.window.setTitle("Pong juego")

WINDOW_WIDTH=1280
WINDOW_HEIGHT=720

VIRTUAL_WIDTH=432
VIRTUAL_HEIGHT=243

PADDLE_SPEED=200




function love.load()
    love.graphics.setDefaultFilter('nearest','nearest')

    servingPlayer = 1

    ---ERRORES 
    ErrorPlayer1=0
    ErrorPlayer2=0


    sFont = love.graphics.newFont('font.ttf',8)
    scoreFont = love.graphics.newFont('font.ttf',32)
    math.randomseed(os.time())
    
    sounds = {
        ['paddle_hit'] = love.audio.newSource('sounds/paddle_hit.wav', 'static'),
        ['score'] = love.audio.newSource('sounds/score.wav', 'static'),
        ['wall_hit'] = love.audio.newSource('sounds/wall_hit.wav', 'static')
    }

    
    push:setupScreen(VIRTUAL_WIDTH,VIRTUAL_HEIGHT,WINDOW_WIDTH,WINDOW_HEIGHT,{
        fullscreen = false,
        resizable = true,
        vsync = true
    })
    
    player1 =Paddle(10,30,5,20)
    player2 =Paddle(VIRTUAL_WIDTH-10,VIRTUAL_HEIGHT-30,5,20)

    
    player1Score = 0
    player2Score = 0

    
    ball=Ball(VIRTUAL_WIDTH/2-4,VIRTUAL_HEIGHT/2-4,4,4)


    gameState='start'
    
end

function love.resize(w, h)
    push:resize(w, h)
end

function love.keypressed(key)

    
    if gameState== 'start' then
        if key== 'c' then
            player1.AUTO=true
            gameState='select'
        elseif key =='a' then
            player1.AUTO=true
            player2.AUTO=true
            gameState='serve'
        end
    end

    if gameState=='select' then
        if key == 'l' then
            player1.DIFICULT="LOW"
            gameState='serve'
        elseif key=='m'then
            player1.DIFICULT="MEDIUM"
            gameState='serve'
        elseif key=='H'then
            player1.DIFICULT="HARD"
            gameState='serve'
        elseif key=='I'then
            player1.DIFICULT="IMPOSIBLE"
            gameState='serve'
        end
    end

    if key == 'escape' then
        love.event.quit()
    elseif key == 'enter' or key == 'return' then
        if gameState == 'start' then
            gameState = 'serve'
        elseif gameState == 'serve' then
            gameState = 'play'
        elseif gameState == 'done' then
            gameState = 'serve'

            ball:reset()

            player1Score = 0
            player2Score = 0


            if winningPlayer == 1 then
                servingPlayer = 2
            else
                servingPlayer = 1
            end
        end
    end
end


function love.update(dt)
    if gameState == 'serve' then
        -- before switching to play, initialize ball's velocity based
        -- on player who last scored
        ball.dy = math.random(-50, 50)
        if servingPlayer == 1 then
            ball.dx = math.random(140, 200)
        else
            ball.dx = -math.random(140, 200)
        end
    elseif gameState == 'play' then
        if ball:collides(player1) then
            ball.dx= -ball.dx *1.03
            ball.x=player1.x + 5
            if ball.dy<0 then
                ball.dy = -math.random( 10, 150)
            else
                ball.dy=math.random( 10,150 )
            end

            ErrorPlayer1=player1:ErrorRandom()
            sounds['paddle_hit']:play()
            


        end
        
        if ball:collides(player2) then
            ball.dx = -ball.dx * 1.03
            ball.x = player2.x - 4
            
            -- keep velocity going in the same direction, but randomize it
            if ball.dy < 0 then
                ball.dy = -math.random(10, 150)
            else
                ball.dy = math.random(10, 150)
            end
            
            ErrorPlayer2=player2:ErrorRandom()
            
            sounds['paddle_hit']:play()


        end

        if ball.y <= 0 then
            ball.y = 0
            ball.dy = -ball.dy
            sounds['wall_hit']:play()
            ErrorPlayer1=0
            ErrorPlayer2=0
        end

        if ball.y >= VIRTUAL_HEIGHT - 4 then
            ball.y = VIRTUAL_HEIGHT - 4
            ball.dy = -ball.dy
            ErrorPlayer2=0
            ErrorPlayer1=0
            sounds['wall_hit']:play()
        end
    end
    
    if ball.x < 0 then
        servingPlayer = 1
        player2Score = player2Score + 1
        sounds['score']:play()

        -- if we've reached a score of 10, the game is over; set the
        -- state to done so we can show the victory message
        if player2Score == 10 then
            winningPlayer = 1
            gameState = 'done'
            ball:reset()
        else
            gameState = 'serve'
            -- places the ball in the middle of the screen, no velocity
            ball:reset()
        end
    end

    if ball.x > VIRTUAL_WIDTH then
        servingPlayer = 2
        player1Score = player1Score + 1
        sounds['score']:play()
        
        if player1Score == 10 then
            winningPlayer = 2
            gameState = 'done'
            ball:reset()
        else
            gameState = 'serve'
            ball:reset()
        end
    end

    -- player 1 movement
    player1:movements(dt,ball.dy,gameState,PADDLE_SPEED,ErrorPlayer1,"w","s")
    
    
    -- player 2 movement
    player2:movements(dt,ball.dy,gameState,PADDLE_SPEED,ErrorPlayer2,"up","down")

    

    if gameState == 'play' then
        ball:update(dt)
    end

    player1:update(dt)
    player2:update(dt)
end




function love.draw()
    
    push:apply("start") 

    --for version 11.3 any other lest without /255

    love.graphics.clear(40/255, 45/255, 52/255, 255/255)
    love.graphics.setFont(sFont)
    --love.graphics.printf('Hello Pong!', 0, 20, VIRTUAL_WIDTH, 'center')
    
    displayScore()

    if gameState == 'start' then
        love.graphics.setFont(sFont)
        love.graphics.printf('Welcome to Pong!', 0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Press Enter to play Multiplayer \nPress C for play with Computer \nPress A for see autoplay ...begin!', 0, 20, VIRTUAL_WIDTH, 'center')
    
    elseif gameState == 'serve' then
        love.graphics.setFont(sFont)
        love.graphics.printf('Player ' .. tostring(servingPlayer) .. "'s serve!", 
            0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.printf('Press Enter to serve!', 0, 20, VIRTUAL_WIDTH, 'center')
    elseif gameState == 'play' then
    
    elseif gameState =='select' then
        love.graphics.setFont(sFont)
        love.graphics.printf('Press \nL: for low level\nM: for medium level\nH: for Hard level\nI: for imposible level its really imposible!!', 0, 20, VIRTUAL_WIDTH, 'center')

        -- no UI messages to display in play
    elseif gameState == 'done' then
        -- UI messages
        love.graphics.setFont(sFont)
        love.graphics.printf('Player ' .. tostring(winningPlayer) .. ' wins!',
            0, 10, VIRTUAL_WIDTH, 'center')
        love.graphics.setFont(sFont)
        love.graphics.printf('Press Enter to restart!', 0, 30, VIRTUAL_WIDTH, 'center')
    end 



    --player1
    player1:render()
    --player2
    
    player2:render()
    ---ball
    ball:render()

    displayFPS()

    push:apply("end")

end

function displayFPS()
    -- simple FPS display across all states
    love.graphics.setFont(sFont)
    love.graphics.setColor(0, 255, 0, 255)
    love.graphics.print('FPS: ' .. tostring(love.timer.getFPS()), 10, 10)
end


function displayScore()
    -- draw score on the left and right center of the screen
    -- need to switch font to draw before actually printing
    love.graphics.setFont(scoreFont)
    love.graphics.print(tostring(player1Score), VIRTUAL_WIDTH / 2 - 50, 
        VIRTUAL_HEIGHT / 3)
    love.graphics.print(tostring(player2Score), VIRTUAL_WIDTH / 2 + 30,
        VIRTUAL_HEIGHT / 3)
end