-- @Author: Your name
-- @Date:   2020-04-13 22:31:17
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-19 00:02:16
Paddle=Class{}



function Paddle:init( x,y,width, height)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.dy = 0
    self.AUTO=false
    self.DIFICULT="IMPOSIBLE"
    
end





function Paddle:update( dt )
    if self.dy < 0 then
        self.y = math.max(0, self.y + self.dy * dt)
    else
        self.y = math.min(VIRTUAL_HEIGHT - self.height, self.y + self.dy * dt)
    end
end

function Paddle:render()
    love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
end


function Paddle:ErrorRandom()
    switch = {
        ['LOW'] = function()	-- for case 1 50%
            p= math.random( 10 ) <=5 and 0 or 10;
            return p;
        end,
        ['MEDIUM'] = function()	-- for case 2 30%
            p= math.random( 10 ) <= 6 and 0 or 10;
            return p;
        end,
        ['HARD'] = function()	-- for case 3 10%
            p= math.random( 10 ) <= 9 and 0 or 10 
            return p;
        end,
        ['IMPOSIBLE'] = function()	-- for case 3 0% --but velocity x do erro an do points
            return 0;
        end
    }
    
    f=switch[self.DIFICULT]
    if (f) then
        return f()
    else
        return 0
    end
end


function Paddle:movements(dt,dy,gameState,SPEED,Error,KeyUp,KeyDown)
    if self.AUTO then
        if gameState =='serve' then
            self.y =ball.y

        elseif gameState =='play' then
            
            self.dy =dy+Error
            --p=math.random( 4 ) > 1 and dy or dy+10
            
        end
    else
        if love.keyboard.isDown(KeyUp) then
            self.dy = -SPEED
        elseif love.keyboard.isDown(KeyDown) then
            self.dy = SPEED
        else
            self.dy=0
        end
    end
end