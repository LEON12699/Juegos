-- @Author: Your name
-- @Date:   2020-04-19 17:45:00
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-24 21:31:11

push = require("push")
Class = require "class"
require "Bird"
require "Pipe"
require 'PipePair'
require "StateMachine"
require "states/BaseState"
require "states/PlayState"
require "states/ScoreState"
require "states/TitleScreenState"
require "states/CountdownState"

WINDOW_WIDTH=1280
WINDOW_HEIGTH=720

VIRTUAL_WIDTH = 512
VIRTUAL_HEIGHT= 288


local background = love.graphics.newImage('images/background.png')
local ground = love.graphics.newImage('images/ground.png')

local backgroundScroll=0
local groundScroll=0

local BACKGROUND_SCROLL_SPEED=30
local GROUND_SCROLL_SPEED=60
local BACKGROUND_LOOPING_POINT=413

local bird =Bird()

local pipePairs = {}

local spawnTimer=0

local lastY = -PIPE_HEIGHT + math.random(80) + 20

local scrolling = true

function love.load(  )
    love.graphics.setDefaultFilter('nearest','nearest')
    math.randomseed(os.time())
    love.window.setTitle('flypy')
    
    smallFont= love.graphics.newFont('fonts/font.ttf',8)
    mediumFont= love.graphics.newFont('fonts/font.ttf',14)
    flappyFont= love.graphics.newFont('fonts/flappy.ttf',28)
    hugeFont= love.graphics.newFont('fonts/flappy.ttf',56)



    love.graphics.setFont(flappyFont)




    push:setupScreen(VIRTUAL_WIDTH,VIRTUAL_HEIGHT,WINDOW_WIDTH,WINDOW_HEIGTH,{
        vsync=true,
        fullscreen=false,
        resizable=true
    })



    sounds = {
        ['jump'] = love.audio.newSource('sounds/jump.wav', 'static'),
        ['explosion'] = love.audio.newSource('sounds/explosion.wav', 'static'),
        ['hurt'] = love.audio.newSource('sounds/hurt.wav', 'static'),
        ['score'] = love.audio.newSource('sounds/score.wav', 'static'),

        -- https://freesound.org/people/xsgianni/sounds/388079/
        ['music'] = love.audio.newSource('sounds/marios_way.mp3', 'static')
    }

    -- kick off music
    sounds['music']:setLooping(true)
    sounds['music']:play()

    
    gStateMachine = StateMachine{
        ['title']= function() return TitleScreenState() end,
        ['play']=function() return PlayState() end,
        ['score']=function() return ScoreState() end,
        ['countdown']=function() return CountdownState() end,

    }
    gStateMachine:change('title')

    love.mouse.buttonsPressed = {}
    
    love.keyboard.keysPressed={}
end

function love.resize(v,h)
    push:resize(v,h)
end

function love.keypressed(key)
    love.keyboard.keysPressed[key]=true
    if key=="escape" then 
        love.event.quit()
    end
end

function love.mousepressed(x, y, button)
    love.mouse.buttonsPressed[button] = true
end


function love.mouse.wasPressed(button)
    return love.mouse.buttonsPressed[button]
end

function love.keyboard.wasPressed( key )
    if love.keyboard.keysPressed[key] then
        return true
    else
        return false
    end
end

function love.update(dt )
    backgroundScroll=(backgroundScroll+BACKGROUND_SCROLL_SPEED*dt) % BACKGROUND_LOOPING_POINT

    groundScroll=(groundScroll+GROUND_SCROLL_SPEED*dt) % VIRTUAL_WIDTH

    gStateMachine:update(dt)
    love.keyboard.keysPressed={}
    love.mouse.buttonsPressed = {}
    

end

function love.draw(  )
    push:start()
        love.graphics.draw(background,-backgroundScroll,0)

        gStateMachine:render()
       -- for k, pipe in pairs(pipePairs) do 
       --     pipe:render()
       -- end
        love.graphics.draw(ground,-groundScroll,VIRTUAL_HEIGHT-16)
    
    push:finish()
end