-- @Author: Your name
-- @Date:   2020-04-23 00:54:09
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-23 01:33:13

TitleScreenState=Class{__includes = BaseState}

function TitleScreenState:update( dt )
    if love.keyboard.wasPressed('enter') or love.keyboard.wasPressed('return') then
        gStateMachine:change('countdown')
    end
end

function TitleScreenState:render()
    love.graphics.setFont(flappyFont)
    love.graphics.printf('Flapy', 0, 64, VIRTUAL_WIDTH, 'center')

    love.graphics.setFont(mediumFont)
    love.graphics.printf('Press Enter', 0, 100, VIRTUAL_WIDTH, 'center')
end
