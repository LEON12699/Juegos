-- @Author: Your name
-- @Date:   2020-04-23 01:19:23
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-24 21:27:48


ScoreState = Class{__includes = BaseState}

local medals_image = love.graphics.newImage('images/medalla.png')

function ScoreState:enter(params)
    self.score = params.score
end

function ScoreState:update(dt)
    -- go back to play if enter is pressed
    if love.keyboard.wasPressed('enter') or love.keyboard.wasPressed('return') then
        gStateMachine:change('countdown')
    end
end

function ScoreState:render()
    -- simply render the score to the middle of the screen
    love.graphics.setFont(flappyFont)
    love.graphics.printf('Oof! You lost!', 0, 64, VIRTUAL_WIDTH, 'center')

    love.graphics.setFont(mediumFont)
    love.graphics.printf('Score: ' .. tostring(self.score), 0, 100, VIRTUAL_WIDTH, 'center')
    
    
    love.graphics.draw(medals_image,VIRTUAL_WIDTH/(2)-medals_image:getWidth()/2,120)
    
    
    

    love.graphics.printf('Press Enter to Play Again!', 0, 210, VIRTUAL_WIDTH, 'center')
end
