-- @Author: Your name
-- @Date:   2020-04-19 18:54:14
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-24 21:28:50

Bird =Class{}

local GRAVITY= 18

function Bird:init(  )
    self.image= love.graphics.newImage('images/bird.png')
    self.width = self.image:getWidth()
    self.height = self.image:getHeight()
    self.dy=0
    self.x = VIRTUAL_WIDTH/2- (self.width/2)
    self.y= VIRTUAL_HEIGHT/2 -(self.height/2)
    
end

function Bird:collides( pipe )
    -- (self.x + 2) + (self.width - 5) this line represents de collide box border rigth
    --(self.y + 2) + (self.height - 4) this lines represents the collide box buttom
    if (self.x + 2) + (self.width - 5) >= pipe.x and self.x + 2 <= pipe.x + PIPE_WIDTH then
        if (self.y + 2) + (self.height - 5) >= pipe.y and self.y +2  <= pipe.y + PIPE_HEIGHT then
            return true
        end
    end

    return false    
end


function Bird:update( dt )
    self.dy =self.dy+ GRAVITY *dt
    
    if love.keyboard.wasPressed('space') or love.mouse.wasPressed(1) then
        self.dy = -4.5
        sounds['jump']:play()
    end
    self.y = self.y+self.dy

end

function Bird:render(  )
    love.graphics.draw(self.image,self.x,self.y)
    
end