-- @Author: Your name
-- @Date:   2020-04-22 23:06:26
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-23 23:39:09
PipePair= Class{}
-- size between

--local GAP_HEIGHT =90

function PipePair:init( y ,GAP_HEIGHT)
    self.x = VIRTUAL_WIDTH +32
    self.y = y 
    self.GAP_HEIGHT=GAP_HEIGHT or 90

    self.pipes={
        ['upper'] =Pipe('top',self.y),
        ['lower']=Pipe('bottom',self.y + PIPE_HEIGHT + self.GAP_HEIGHT)
    }

    self.remove=false
    self.scored=false
end

function PipePair:update( dt )
    if self.x > -PIPE_WIDTH then
        self.x= self.x -PIPE_SCROLL *dt
        self.pipes['lower'].x=self.x
        self.pipes['upper'].x=self.x
    else
        self.remove=true
    end
end

function PipePair:render(  )
    for k,pipe in pairs(self.pipes) do
        pipe:render()
    end
end