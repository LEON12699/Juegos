-- @Author: Your name
-- @Date:   2020-04-19 21:23:36
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-24 21:27:55

Pipe= Class{}

local PIPE_IMAGE = love.graphics.newImage('images/pipe.png')

PIPE_SCROLL= 60

PIPE_HEIGHT=288
PIPE_WIDTH=70

function Pipe:init(orientation,y)
    self.x=VIRTUAL_WIDTH
    self.y= y--math.random( VIRTUAL_HEIGHT/4,VIRTUAL_WIDTH-10 )
    self.width= PIPE_IMAGE:getWidth()
    self.orientation=orientation
end

function Pipe:update( dt )
    
end

function Pipe:render()
    love.graphics.draw(PIPE_IMAGE,self.x,
    (self.orientation == 'top' and self.y +PIPE_HEIGHT or self.y),
    0, --rotation
    1, --x-scale
    self.orientation=='top' and -1 or 1 --y scale

)

end