-- @Author: Your name
-- @Date:   2020-04-23 00:46:25
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-24 23:27:51
StateMachine= Class{}

function StateMachine:init( states )
    self.empty={
        render = function() end,
        update = function() end,
        enter = function() end,
        exit = function() end
    }

    self.states= states or {}
    self.current = self.empty
end

function StateMachine:change( stateName,enterParams )
    assert(self.states[stateName],"state name not define")
    self.current:exit()
    self.current=self.states[stateName]()
    self.current:enter(enterParams)

end

function StateMachine:update( dt )
    self.current:update(dt)
end

function StateMachine:render(  )
    self.current:render()
end