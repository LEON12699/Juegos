-- @Author: Your name
-- @Date:   2020-04-24 23:22:04
-- @Last Modified by:   Your name
-- @Last Modified time: 2020-04-24 23:25:41
BaseState = Class{}

function BaseState:init() end
function BaseState:enter() end
function BaseState:exit() end
function BaseState:update(dt) end
function BaseState:render() end